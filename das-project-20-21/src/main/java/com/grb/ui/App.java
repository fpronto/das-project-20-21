package com.grb.ui;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.grb.dao.UserRepository;
import com.grb.entity_manager.EntityManagerConfig;
import com.grb.model.*;
import com.grb.state.StateMachine;

public class App extends JFrame {
    private JPanel appPanel;
    private JButton exportButton;
    private JButton inserirReferênciaButton;
    private JComboBox UsersComboBox;
    private JComboBox comboBox2;
    private JButton carregarReferênciasButton;
    private JButton gerirReferênciasButton;
    private JButton undoButton;
    private JTextArea textArea1;
    private JButton redoButton;
    private JButton addUser;
    private JComboBox DocumentsComboBox;
    private JButton saveButton;
    private JTextField documentNameField;
    private JLabel documentNameLabel;
    private JButton clearTextArea;
    CommandManager cmd;
    StateMachine sm;

    public App(StateMachine sm) {
        this.sm = sm;
        DefaultComboBoxModel<String> userListModel = new DefaultComboBoxModel<>();
        DefaultComboBoxModel<String> documentListModel = new DefaultComboBoxModel<>();
        UsersComboBox.setModel(userListModel);
        DocumentsComboBox.setModel(documentListModel);

        UserRepository userRepository = new UserRepository();
        ArrayList<String> users = userRepository.getAllUsers();
        userListModel.addElement("Selecione Utilizador");
        for (int i = 0; i < users.size(); i++) {
            userListModel.addElement(users.get(i));
        }
        sm.setUsersListModel(userListModel);
        UsersComboBox.setSelectedIndex(0);

        documentListModel.addElement("Selecione Documento");
        for (int i = 0; i<sm.getDm().getDocumentsListModel().getSize(); i++){
            documentListModel.addElement(sm.getDm().getDocumentsListModel().getElementAt(i).toString());
        }
        sm.setDocumentsListModel(documentListModel);
        DocumentsComboBox.setSelectedIndex(0);

        add(appPanel);
        setTitle("Gestão de Referências Bibliográficas");
        pack();
        centreWindow(this);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);   //EXIT_ON_CLOSE
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                sm.getDm().getUnitOfWork().commit();
                EntityManagerConfig.getEntityManager().close();
                System.exit(0);
            }
        });
        saveButton.setEnabled(false);
        carregarReferênciasButton.setEnabled(false);
        textArea1.setEditable(false);
        exportButton.setEnabled(false);
        comboBox2.addItem("IEEE");
        comboBox2.addItem("APA");
        comboBox2.addItem("ACM");

        gerirReferênciasButton.addActionListener(actionEvent -> {
            sm.openManageReferences();
        });
        inserirReferênciaButton.addActionListener(actionEvent -> {
            sm.openInsertReferences();
            carregarReferênciasButton.setEnabled(false);
        });
        addUser.addActionListener(actionEvent -> {
            sm.openAddUser();
            pack();
        });

        carregarReferênciasButton.addActionListener(actionEvent -> {
            try {
                sm.loadReferences(this);
                gerirReferênciasButton.setEnabled(true);
                inserirReferênciaButton.setEnabled(true);
                textArea1.setEditable(true);
            } catch (Exception e) {
                gerirReferênciasButton.setEnabled(false);
                inserirReferênciaButton.setEnabled(false);
            }
        });

        textArea1.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                UsersComboBox.setEnabled(false);
                DocumentsComboBox.setEnabled(false);
                saveButton.setEnabled(true);
                inserirReferênciaButton.setEnabled(true);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                UsersComboBox.setEnabled(false);
                DocumentsComboBox.setEnabled(false);
                saveButton.setEnabled(true);
                inserirReferênciaButton.setEnabled(true);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                UsersComboBox.setEnabled(false);
                DocumentsComboBox.setEnabled(false);
                saveButton.setEnabled(true);
                inserirReferênciaButton.setEnabled(true);
            }
        });

        exportButton.addActionListener(ActionEvent -> {
            Text text = new Text(textArea1.getText(),sm, this);
            String type = String.valueOf(comboBox2.getSelectedItem());
            sm.exportAction(text, type);
            DocumentsComboBox.setModel(sm.getDm().getDocumentsListModel());
        });
        undoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sm.undoAction(textArea1);
                if (sm.existsUndo() == 0) {
                    undoButton.setEnabled(false);
                }
                redoButton.setEnabled(true);
            }
        });
        redoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sm.redoAction(textArea1);
                if (sm.existsUndo()== 0) {
                    redoButton.setEnabled(false);
                }
                undoButton.setEnabled(true);
            }
        });

        textArea1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                sm.clearCmd();
                redoButton.setEnabled(false);
                undoButton.setEnabled(false);
            }
        });
        UsersComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = UsersComboBox.getSelectedItem().toString();
                User user = sm.getDm().getUser();


                if ((user == null || !name.equals(user.getName())) && !name.equals("Selecione Utilizador")){
                    UserRepository userRepository = new UserRepository();
                    user = userRepository.getUser(name);
                    user.setDocument(removeDuplicates(user));
                    sm.getDm().setUser(user);

                    for(int i=sm.getDm().getDocumentsListModel().getSize()-1;i>=1;i--){
                        sm.getDm().getDocumentsListModel().removeElementAt(i);
                    }

                    for (int i = 0; i<user.getDocument().size(); i++){
                        sm.getDm().getDocumentsListModel().addElement(user.getDocument().get(i).getName());
                    }
                    DocumentsComboBox.setModel(sm.getDm().getDocumentsListModel());
                    DocumentsComboBox.setSelectedIndex(0);
                    sm.getDm().getUserListModel().removeElement("Selecione Utilizador");
                    carregarReferênciasButton.setEnabled(true);
                    textArea1.setEditable(true);
                    exportButton.setEnabled(true);
                    return;
                }
                if (name.equals("Selecione Utilizador")) {
                    for(int i=DocumentsComboBox.getItemCount()-1;i>=1;i--){ //DocumentsComboBox.removeAllItems() not working...
                        DocumentsComboBox.removeItemAt(i);
                    }
                    return;
                }
            }
        });

        DocumentsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String documentName = DocumentsComboBox.getSelectedItem().toString();
                User user = sm.getDm().getUser();
                for (int i = 0; i<user.getDocument().size(); i++){
                    if (user.getDocument().get(i).getName().equals(documentName)){
                        String text = getFileText(user.getDocument().get(i).getDirectory());
                        if (text == null) {
                            disableElements();
                            return;
                        }
                        checkReferenceFileExists(user.getDocument().get(i).getReferenceDirectory(), text);
                        documentNameField.setText(documentName);
                    }
                }
            }
        });
        saveButton.addActionListener(actionEvent -> {
                if (documentNameField.getText().trim().isEmpty() || documentNameField.getText() == null || documentNameField.getText().contains(" ")){
                    documentNameLabel.setBackground(Color.red);
                    documentNameLabel.setOpaque(true);
                    pack();
                    return;
                }
                documentNameLabel.setOpaque(false);
                documentNameLabel.repaint();
                pack();
                sm.getDm().setText(new Text(textArea1.getText(),sm, this));
                sm.getDm().setFilename(documentNameField.getText()+".txt");
                OriginalFile createOriginalFile = new OriginalFile();
                createOriginalFile.prepareOriginalDoc(sm.getDm());
                UsersComboBox.setEnabled(true);
                DocumentsComboBox.setEnabled(true);
                DocumentsComboBox.setModel(sm.getDm().getDocumentsListModel());
        });
        clearTextArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea1.setText(" ");
                UsersComboBox.setEnabled(true);
                DocumentsComboBox.setEnabled(true);
                carregarReferênciasButton.setEnabled(true);
                inserirReferênciaButton.setEnabled(false);
                documentNameField.setText(" ");
                DocumentsComboBox.setSelectedIndex(0);
            }
        });
    }

    public String[] getDocuments(ArrayList<Document> documents){
        String[] array = new String[documents.size()];
        for(int i = 0; i < array.length; i++) {
            array[i] = documents.get(i).getName();
        }
        return array;
    }

    public int getCurrentPosition(){
        return textArea1.getCaretPosition();
    }

    public JTextArea getTextArea(){
        return textArea1;
    }

    public void insertReference(int c, String s){
        this.sm.addToUndo(c,s);
        undoButton.setEnabled(true);
        textArea1.insert(s, c);
    }


    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(950, 500);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public String getFileText(String filePath){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            String ls = System.getProperty("line.separator");
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            reader.close();

            String content = stringBuilder.toString();
            return content;
        } catch (Exception exception) {
            return null;
        }
    }

    public void enableElements(String text){
        textArea1.setText(null);
        textArea1.setText(text);
        textArea1.setEditable(true);
    }

    public void disableElements(){
        gerirReferênciasButton.setEnabled(false);
        inserirReferênciaButton.setEnabled(false);
        DocumentsComboBox.setSelectedIndex(0);
    }

    public void checkReferenceFileExists(String referenceFilePath, String text){
        try {
            if (referenceFilePath == null){
                enableElements(text);
                carregarReferênciasButton.setEnabled(true);
                gerirReferênciasButton.setEnabled(false);
                inserirReferênciaButton.setEnabled(false);
                return;
            }
            sm.loadReferences(referenceFilePath);
            enableElements(text);
            carregarReferênciasButton.setEnabled(false);
            gerirReferênciasButton.setEnabled(true);
            inserirReferênciaButton.setEnabled(true);
        } catch (Exception e) {
            disableElements();
        }
    }

    public List<Document> removeDuplicates(User user){
        ArrayList<Document> documents = new ArrayList<>();
        for (int i = 0; i<user.getDocument().size(); i++){
            if (i>0){
                if (!user.getDocument().get(i).getName().equals(user.getDocument().get(i-1).getName()) &&
                        !user.getDocument().get(i).getDirectory().equals(user.getDocument().get(i-1).getDirectory())){
                    documents.add(user.getDocument().get(i));
                }
            }
            else {
                documents.add(user.getDocument().get(i));
            }
        }
        return documents;
    }
}

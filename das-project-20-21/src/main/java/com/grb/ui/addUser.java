package com.grb.ui;

import com.grb.model.User;
import com.grb.state.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class addUser extends JFrame{
    private JTextField nameField;
    private JButton createButton;
    private JPanel addUserJPanel;
    private JLabel alertLabel;

    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }
    public addUser(final JFrame parent,  StateMachine sm) {
        add(addUserJPanel);
        alertLabel.setVisible(false);
        setTitle("Criar Novo Utilizador");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        centreWindow(this);
        createButton.addActionListener(actionEvent -> {
            DefaultComboBoxModel aux = sm.getDm().getUserListModel();
            if(aux.getIndexOf(nameField.getText()) != -1){
                alertLabel.setVisible(true);
                alertLabel.setText("Utilizador já existe!");
                alertLabel.setBackground(Color.red);
                pack();
                return;
            }
            if (nameField.getText().trim().isEmpty() || nameField.getText() == null){
                alertLabel.setVisible(true);
                alertLabel.setText("Campos Incorretos ou em Falta!");
                alertLabel.setBackground(Color.red);
                pack();
                return;
            }
            User user = new User();
            user.setName(nameField.getText());
            user.setDocument(new ArrayList<>());
            sm.ok(user);
            setVisible(false);
            return;
        });

        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosed(WindowEvent e)
            {
                sm.close();
            }
        });
        setVisible(true);
    }
}

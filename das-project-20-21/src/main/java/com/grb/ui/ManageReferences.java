package com.grb.ui;

import com.grb.model.DataModel;
import com.grb.model.Reference;
import com.grb.state.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class ManageReferences extends JFrame {
    private JList<String> list1;
    private JPanel managerReferencesPanel;
    private JButton adicionarButton;
    private JButton modificarButton;
    private JButton eliminarButton;
    private JLabel alertLabel;
    DefaultListModel<String> model = new DefaultListModel<String>();

    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    public ManageReferences(final JFrame parent,  StateMachine sm) {
        // super(parent, true);
        ArrayList referenceList = sm.getDm().getReferenceList();
        sm.setModel(model);
        list1.setModel(model);
        for (int i = 0; i < referenceList.size(); i++) {
            model.addElement(referenceList.get(i).toString());
        }
        add(managerReferencesPanel);
        alertLabel.setVisible(false);
        setTitle("Gestão de Referências Bibliográficas - Gerir Referencia");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        centreWindow(this);
        setVisible(true);
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosed(WindowEvent e)
            {
                sm.close();
            }
        });

        adicionarButton.addActionListener(actionEvent -> {
            sm.openAddReference();
        });

        modificarButton.addActionListener(actionEvent -> {

            int index = list1.getSelectedIndex();
            if (index < 0){
                alertLabel.setVisible(true);
                alertLabel.setText("Selecione referência que deseja modificar!!");
                return;
            }

            sm.openModifyReference(index);
        });

        eliminarButton.addActionListener(actionEvent -> {
            int index = list1.getSelectedIndex();
            if (index < 0){
                alertLabel.setVisible(true);
                alertLabel.setText("Selecione referência que deseja apagar!!");
                return;
            }
            sm.deleteReference(index);
        });


    }
}

package com.grb.ui;

import com.grb.model.CommandManager;
import com.grb.state.StateMachine;
import org.h2.command.Command;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class InsertReferences  extends JFrame{
    private JPanel insertReferencesPanel;
    private JList list1;
    private JLabel alertLabel;
    private JPanel InsertReferences;
    private JButton inserirButton;
    DefaultListModel<String> model = new DefaultListModel<String>();

    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    public InsertReferences(final JFrame parent,  StateMachine sm) {
        // super(parent, true);
        ArrayList referenceList = sm.getDm().getReferenceList();
        sm.setModel(model);
        list1.setModel(model);
        for (int i = 0; i < referenceList.size(); i++) {
            model.addElement(referenceList.get(i).toString());
        }
        model.addElement("Inserir Lista");
        add(insertReferencesPanel);
        alertLabel.setVisible(false);
        setTitle("Inserir referências");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        centreWindow(this);
        setVisible(true);
        sm.setInsert(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent e) {
                sm.close();
            }
        });


        inserirButton.addActionListener(actionEvent -> {
            int index = list1.getSelectedIndex();
            sm.insertReference(index,sm.getCurrentPosition());
            setVisible(false);
        });
    }

    public JList getList(){
        return list1;
    }


}


package com.grb.ui;

import com.grb.model.Reference;
import com.grb.model.SourceType;
import com.grb.state.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.atomic.AtomicReference;

public class ModifyReference extends JDialog {
    private JButton modificarButton;
    private JTextField referenceCodeField;
    private JTextField authorField;
    private JTextField ArticleNameField;
    private JTextField sourceField;
    private JTextField yearField;
    private JPanel modifyReferencePanel;
    private JComboBox comboBox1;

    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    public ModifyReference(final JFrame parent, StateMachine sm) {
        super(parent, true);
        //setModal(true);
        add(modifyReferencePanel);
        setTitle("Gestão de Referências Bibliográficas - Modificar Referencia");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        comboBox1.setModel(new DefaultComboBoxModel(SourceType.values()));
        AtomicReference<Reference> reference = new AtomicReference<>(sm.getReferenceToModify());
        setFieldText(reference.get());
        centreWindow(this);
        pack();

        modificarButton.addActionListener(actionEvent -> {
            reference.set(updateObject(reference.get().getIndex()));
            sm.ok(reference.get());
            setVisible(false);
            return;
        });
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosed(WindowEvent e)
            {
                sm.close();
            }
        });
        setVisible(true);
    }


    public Reference updateObject(int referenceIndex){
        Reference reference = new Reference.ReferenceBuilder()
                .index(referenceIndex)
                .referenceCode(referenceCodeField.getText())
                .authors(authorField.getText())
                .articleName(ArticleNameField.getText())
                .sourceType(SourceType.valueOf(comboBox1.getSelectedItem().toString()))
                .source(sourceField.getText())
                .year(yearField.getText())
                .build();
        return reference;
    }

    public void setFieldText(Reference reference){
        referenceCodeField.setText(reference.getReferenceCode());
        authorField.setText(reference.getAuthors());
        ArticleNameField.setText(reference.getArticleName());
        sourceField.setText(reference.getSource());
        yearField.setText(reference.getYear());
    }

}

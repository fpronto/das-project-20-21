package com.grb.ui;

import com.grb.model.Reference;
import com.grb.model.SourceType;
import com.grb.state.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AddReference extends JDialog{
    private JTextField yearField;
    private JTextField referenceCodeField;
    private JTextField authorField;
    private JTextField articleNameField;
    private JComboBox comboBox1;
    private JTextField sourceField;
    private JButton adicionarButton;
    private JLabel labelAlert;
    private JPanel addReferencePanel;

    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }


    public AddReference(final JFrame parent, StateMachine sm){
        //super(parent, true);
        add(addReferencePanel);
        setTitle("Gestão de Referências Bibliográficas - Adicionar Referencia");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        centreWindow(this);
        comboBox1.setModel(new DefaultComboBoxModel(SourceType.values()));
        adicionarButton.addActionListener(actionEvent -> {
            if (verifyCompleteFields() == false){
                labelAlert.setVisible(true);
                labelAlert.setText("Campos Incorretos ou em Falta!!");
                labelAlert.setBackground(Color.red);
                return;
            }
            Reference ref = createObject();
            sm.ok(ref);
            setVisible(false);
            return;
        });
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosed(WindowEvent e)
            {
                sm.close();
            }
        });

        setVisible(true);
    }

    public Reference createObject(){
        Reference reference = new Reference.ReferenceBuilder()
                .referenceCode(referenceCodeField.getText())
                .authors(authorField.getText())
                .articleName(articleNameField.getText())
                .sourceType(SourceType.valueOf(comboBox1.getSelectedItem().toString()))
                .source(sourceField.getText())
                .year(yearField.getText())
                .build();
        return reference;
    }

    public boolean verifyCompleteFields(){
        if (referenceCodeField.getText().trim().isEmpty() || referenceCodeField.getText() == null
                || authorField.getText().trim().isEmpty() || authorField.getText() == null
                || articleNameField.getText().trim().isEmpty() || articleNameField.getText() == null
                || sourceField.getText().trim().isEmpty() || sourceField.getText() == null
                || yearField.getText().trim().isEmpty() || yearField.getText() == null){
            return false;
        }
        return true;
    }
}

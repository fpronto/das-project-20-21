package com.grb.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.grb.state.StateMachine;

public class Style extends JFrame {
    private JCheckBox checkBox1;
    private JCheckBox checkBox2;
    private JButton guardarButton;
    private JPanel Painel;
    private JRadioButton radioButton1;
    private JRadioButton radioButton2;
    private JPanel Itálico;
    private JPanel Negrito;

    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    public Style(final JFrame parent, StateMachine sm) {
        add(Painel);
        setTitle("Escolher tipo de ficheiro e estilos");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        centreWindow(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent e) {
                sm.close();
            }
        });
        guardarButton.addActionListener(actionEvent -> {
            sm.ok(checkBox1.isSelected(), checkBox2.isSelected());
            setVisible(false);
        });
        setVisible(true);

    }
    /*
    public JCheckBox getBold() {
        return checkBox1;
    }

    public JCheckBox getItalic() {
        return checkBox2;
    }
    */

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 150);
    }
}

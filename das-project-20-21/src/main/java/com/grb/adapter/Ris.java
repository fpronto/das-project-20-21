package com.grb.adapter;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import com.grb.model.Reference;
import com.grb.model.SourceType;
import org.apache.commons.lang3.StringUtils;

public class Ris extends ReferenceFile {
    private static final Pattern PATTERN_ITEMSTART = Pattern.compile("^TY  -.*");
    private static final Pattern PATTERN_ITEMEND = Pattern.compile("^ER  -.*");
    private static final Pattern PATTERN_ITEM = Pattern.compile("^[A-Z][A-Z0-9]  -.*");
    private static final Pattern PATTERN_ITEMEXTENSION = Pattern.compile("^      .+");
    Map<String, List<String>> rawData = new HashMap<String, List<String>>();

    public Ris(String filePath) {
        this.filePath = filePath;
    }

    public ArrayList<Reference> parseRisFile() throws Exception {
        try{
            File file = new File(filePath);
            if (file.exists()){
                BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
                RisContentService contentService = new RisContentService(in);
                var referenceList = new ArrayList<Reference>();
                int referenceCounter = 1;
                while (contentService.hasNextLine()) {
                    if(parseNextItem(contentService)){
                        Reference reference = normalize(referenceCounter);
                        referenceList.add(reference);
                    }
                    rawData.clear();
                }
                return referenceList;
            }
        }catch (Exception e){
            throw new Exception();
        }
        return null;
    }

    public String generateReferenceCode(int referenceCounter, String authors){
        if (authors == null || authors.isEmpty()){
            return referenceCounter + " - ";
        }
        String referenceCode = authors.substring(0, authors.indexOf(' '));

        if (referenceCode.contains(",")){
            referenceCode = referenceCode.substring(0, referenceCode.indexOf(','));
        }

        return referenceCounter + " - " + referenceCode;
    }

    public void updateRisReferenceFile(ArrayList<Reference> references) {
        try {
            FileWriter fw = new FileWriter(filePath,false);
            for(int i = 0; i<references.size(); i++){
                fw.write(writeReference(references.get(i)));
                if (i != references.size()-1)
                    fw.write(System.lineSeparator() + System.lineSeparator());
                fw.flush();
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String writeReference(Reference reference){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append("TY  - " + reference.getSourceType().toString() + System.lineSeparator());
        }
        catch (Exception e){
            stringBuilder.append("TY  - null" + System.lineSeparator());
        }
        stringBuilder.append("ID  - " + reference.getReferenceCode() + System.lineSeparator());
        stringBuilder.append("TI  - " + reference.getArticleName() + System.lineSeparator());
        stringBuilder.append("AU  - " + reference.getAuthors() + System.lineSeparator());
        stringBuilder.append("PY  - " + reference.getYear() + System.lineSeparator());
        stringBuilder.append("UR  - " + reference.getSource() + System.lineSeparator());
        stringBuilder.append("ER  - ");
        return stringBuilder.toString();
    }

    private boolean parseNextItem(RisContentService content) throws IOException {

        if (!content.hasNextLine())
            return false;

        // find next line that matches the start of an item
        getNextItemStart(content);
        // until next line matches :
        getItemsUntilItemEnd(content);
        return true;
    }

    private void getNextItemStart(RisContentService content)
            throws IOException {
        while (content.hasNextLine()) {
            String currentLine = content.peekNextLine();
            if (currentLine.trim().length() < 4) {
                content.skipNextLine();
                continue;
            }

            if (PATTERN_ITEMSTART.matcher(StringUtils.stripStart(currentLine, null)).matches()) {
                extractDataFromField(content);
                break;
            }

            content.skipNextLine();
        }
    }

    private void getItemsUntilItemEnd(RisContentService content)
            throws IOException {
        while (content.hasNextLine()) {
            String currentLine = content.peekNextLine();
            if (currentLine.trim().length() < 4) {
                content.skipNextLine();
                continue;
            }

            if (PATTERN_ITEMEND.matcher(StringUtils.stripStart(currentLine, null)).matches()) {
                content.skipNextLine();
                break;
            } else if (PATTERN_ITEM.matcher(StringUtils.stripStart(currentLine, null)).matches()) {
                extractDataFromField(content);
                continue;
            }

            content.skipNextLine();
        }
    }


    private void extractDataFromField(RisContentService content)
            throws IOException {
        // read line
        String fieldName = getFieldName(content);
        String fieldContent = getContent(content);

        addRawFieldData(fieldName, fieldContent);
    }


    private String getContent(RisContentService content) throws IOException {
        // get content
        String line = StringUtils.stripStart(content.getNextLine(), null);
        if (line.length() < 7) {
            return "";
        }

        String fieldContent = line.substring(6);

        // peek next line for continued infos
        while (content.hasNextLine()) {
            String nextLine = content.peekNextLine();

            // if yes, getLine and peek another line
            if (PATTERN_ITEMEXTENSION.matcher(nextLine).matches()) {
                fieldContent = fieldContent + nextLine.substring(6);
                content.skipNextLine();
            } else {
                break;
            }
        }

        return fieldContent;
    }

    private String getFieldName(RisContentService content) throws IOException {
        // get FieldName
        String line = StringUtils.stripStart(content.peekNextLine(), null);
        return line.substring(0, 2);
    }

    public void addRawFieldData(String fieldName, String value) {
        List<String> fieldValues = rawData.get(fieldName);

        if (fieldValues == null) {
            fieldValues = new ArrayList<String>();
        }

        if (value != null && !"".equals(value)) {
            fieldValues.add(value);
        }

        rawData.put(fieldName, fieldValues);
    }

    public String getFirstRawFieldDataFromManyFields(String... tags) {
        for (int i = 0; i < tags.length; i++) {
            List<String> values = getRawFieldData(tags[i]);

            if (null != values && values.size() > 0) {
                return values.get(0);
            }

        }
        return null;
    }

    public String getSingleFirstRawFieldData(String fieldName) {
        List<String> fieldValues = getRawFieldData(fieldName);
        if (fieldValues == null || fieldValues.size() < 1) {
            return null;
        }

        if (fieldName.equals("TY")){
            if (fieldValues.get(0).equals("JOUR")){
                return SourceType.JORNAL.toString();
            }
            if (fieldValues.get(0).equals("BOOK")){
                return SourceType.LIVRO.toString();
            }
            if (fieldValues.get(0).equals("REPORT")){
                return SourceType.RELATÓRIO.toString();
            }
        }

        return fieldValues.get(0);
    }

    public List<String> getRawFieldData(String fieldName) {
        List<String> fieldValues = rawData.get(fieldName);

        if (fieldValues == null) {
            return new ArrayList<String>(); // return an empty list
        }

        return fieldValues;
    }

    public Reference normalize(int referenceCounter) {
        String referenceCode = getSingleFirstRawFieldData(RisFieldDefinitions.FIELD_ID);
        SourceType sourceType = null;
        try{
            sourceType = SourceType.valueOf(getSingleFirstRawFieldData(RisFieldDefinitions.FIELD_TYPE_OF_REFERENCE));
        }catch (IllegalArgumentException e){}

        String articleName = getFirstRawFieldDataFromManyFields(RisFieldDefinitions.FIELD_PRIMARY_TITLE,
                RisFieldDefinitions.FIELD_TITLE);

        String source = getSingleFirstRawFieldData(RisFieldDefinitions.FIELD_URL);

        String authors = getRawFieldData(RisFieldDefinitions.FIELD_AUTHOR).toString();
        authors.substring(1, authors.length() - 1);

        String year = getSingleFirstRawFieldData(RisFieldDefinitions.FIELD_YEAR);

        if (referenceCode == null){
            referenceCode = generateReferenceCode(referenceCounter, authors);
            referenceCounter++;
        }

        Reference reference = new Reference.ReferenceBuilder()
                .referenceCode(referenceCode)
                .sourceType(sourceType)
                .articleName(articleName)
                .source(source)
                .authors(authors)
                .year(year)
                .build();
        return reference;
    }
}
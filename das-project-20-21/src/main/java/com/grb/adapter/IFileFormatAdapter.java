package com.grb.adapter;

import com.grb.model.Reference;

import java.util.ArrayList;

public interface IFileFormatAdapter {
    public ArrayList<Reference> parseFile() throws Exception;
    public void updateReferenceFile(ArrayList<Reference> references);

    public String getFilePath();
}

package com.grb.adapter;

public class FactoryReferenceFile {
    public IFileFormatAdapter getFile(String filePath) throws Exception {
        if (filePath.contains(".bib")){
            return new BibTex(filePath);
        }
        if (filePath.contains(".ris")) {
            return new RisAdapter(new Ris(filePath));
        }
        throw new Exception();
    }
}

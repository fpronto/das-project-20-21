package com.grb.adapter;

import com.grb.model.Reference;

import java.util.ArrayList;

public class RisAdapter implements IFileFormatAdapter{
    private Ris ris;

    public RisAdapter(Ris ris) {
        this.ris = ris;
    }

    @Override
    public ArrayList<Reference> parseFile() throws Exception {
        return ris.parseRisFile();
    }

    @Override
    public void updateReferenceFile(ArrayList<Reference> references) {
        ris.updateRisReferenceFile(references);
    }

    @Override
    public String getFilePath() {
        return ris.filePath;
    }
}

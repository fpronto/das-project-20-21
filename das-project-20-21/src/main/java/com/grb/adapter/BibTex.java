package com.grb.adapter;
import com.grb.model.Reference;
import org.jbibtex.*;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class BibTex extends ReferenceFile implements IFileFormatAdapter{

    public BibTex(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public ArrayList<Reference> parseFile() throws Exception {
        try {
            File file = new File(filePath);
            if (file.exists()) {
                FileReader fr = new FileReader(file);
                BibTeXParser bibtexParser = new BibTeXParser();

                BibTeXDatabase database = bibtexParser.parse(fr);
                Map<Key, BibTeXEntry> entryMap = database.getEntries();
                Collection<BibTeXEntry> entries = entryMap.values();
                ArrayList <Reference> referenceList =  new ArrayList<Reference>();
                for (BibTeXEntry entry : entries) {
                    Value title = entry.getField(BibTeXEntry.KEY_TITLE);
                    Value author = entry.getField(BibTeXEntry.KEY_AUTHOR);
                    Value year = entry.getField(BibTeXEntry.KEY_YEAR);

                    String key = entry.getKey().toString();
                    if (title == null && key == null) {
                        continue;
                    }

                    Reference.ReferenceBuilder referenceBuilder = new Reference.ReferenceBuilder()
                            .referenceCode(key)
                            .articleName(title.toUserString());
                    if (author != null) {
                        referenceBuilder = referenceBuilder.authors(author.toUserString());
                    }
                    if (year != null) {
                        referenceBuilder = referenceBuilder.year(year.toUserString());
                    }
                    Reference reference = referenceBuilder.build();
                    referenceList.add(reference);
                }
                return referenceList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
        return null;
    }

    @Override
    public void updateReferenceFile(ArrayList<Reference> references) {
        try {
            FileWriter fw = new FileWriter(filePath,false);
            for (int i = 0; i <references.size(); i++){
                fw.write(writeReference(references.get(i)));
                fw.flush();
            }
            fw.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String getFilePath() {
        return filePath;
    }

    private String writeReference(Reference reference){
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("@article{" + reference.getReferenceCode() + "," + System.lineSeparator());
            stringBuilder.append("author = {" + reference.getAuthors() + "}," + System.lineSeparator());
            stringBuilder.append("journal = {" + reference.getSource() + "}," + System.lineSeparator());
            stringBuilder.append("title = {{" + reference.getArticleName() + "}}," + System.lineSeparator());
            stringBuilder.append("year = {" + reference.getYear() + "}," + System.lineSeparator());
            stringBuilder.append("}," + System.lineSeparator());
            stringBuilder.append(System.lineSeparator());
            return stringBuilder.toString();
    }
}

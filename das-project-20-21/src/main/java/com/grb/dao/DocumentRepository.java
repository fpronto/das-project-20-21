package com.grb.dao;

import com.grb.entity_manager.EntityManagerConfig;
import com.grb.model.Document;
import com.grb.model.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;

public class DocumentRepository {
    private static final EntityManager entityManager = EntityManagerConfig.getEntityManager();

    public void addDocument(Document document) {
        entityManager.getTransaction().begin();
        entityManager.persist(document);
        entityManager.getTransaction().commit();
    }

    public ArrayList<Document> getUserDocuments(User user){
        Query query = entityManager.createQuery("SELECT e FROM Document e WHERE e.user_id = :id", Document.class);
        return (ArrayList<Document>) query.setParameter("id", user.getId()).getResultList();
    }
}

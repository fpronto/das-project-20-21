package com.grb.dao;
import com.grb.entity_manager.EntityManagerConfig;
import com.grb.model.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private static final EntityManager entityManager = EntityManagerConfig.getEntityManager();

    public ArrayList<String> getAllUsers() {
        Query query = entityManager.createQuery("SELECT e.name FROM User e");
        return (ArrayList<String>) query.getResultList();
    }

    public User getUser(String name){
        Query query = entityManager.createQuery("SELECT e FROM User e WHERE e.name = :name", User.class);
        return (User) query.setParameter("name", name).getSingleResult();
    }

    public void addUser(User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }
}

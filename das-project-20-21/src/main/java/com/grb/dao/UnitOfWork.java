package com.grb.dao;
import com.grb.entity_manager.EntityManagerConfig;
import com.grb.model.Document;
import com.grb.model.User;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class UnitOfWork implements IUnitOfWork<Document>{
    private final ArrayList<Document> newDocument = new ArrayList<>();
    private final ArrayList<Document> modifiedDocument = new ArrayList<>();

    public void registerNew(Document document, User user){
        if (newDocument.contains(document)) {
            return;
        }

        for (Document search : newDocument) {
            if (search.getUser().equals(document.getUser()) && search.getName().equals(document.getName()) && search.getDirectory().equals(document.getDirectory())) {
                search.setReferenceDirectory(document.getReferenceDirectory());
                return;
            }
        }

        List<Document> documents = user.getDocument();
        for (Document search : documents) {
            if (search.getUser().equals(document.getUser()) && search.getName().equals(document.getName()) && search.getDirectory().equals(document.getDirectory())) {
                if ((search.getReferenceDirectory() == null && document.getReferenceDirectory() == null) || search.getReferenceDirectory().equals(document.getReferenceDirectory())) {
                    return;
                }
                registerModified(search, document.getReferenceDirectory());
                return;
            }
        }
        newDocument.add(document);
    }

    public void registerModified(Document document, String referenceDirectory){
        document.setReferenceDirectory(referenceDirectory);
        modifiedDocument.add(document);
    }

    @Override
    public void commit() {
        EntityManager entityManager = EntityManagerConfig.getEntityManager();
        entityManager.getTransaction().begin();
        insertNew(entityManager);
        updateModified(entityManager);
        entityManager.getTransaction().commit();
    }

    public void insertNew(EntityManager entityManager){
        for (Document document : newDocument) {
            entityManager.persist(document);
        }
        newDocument.clear();
    }

    public void updateModified(EntityManager entityManager){
        for (Document document : modifiedDocument) {
            Document search = entityManager.find(Document.class, document.getId());
            search.setReferenceDirectory(document.getReferenceDirectory());
            entityManager.persist(search);
        }
        modifiedDocument.clear();
    }

    public List<Document> getDocsByUser(User user){
        ArrayList<Document> documents = new ArrayList<>();
        for (int i = 0; i<newDocument.size(); i++){
            if (newDocument.get(i).getUser().equals(user)){
                documents.add(newDocument.get(i));
            }
        }
        return documents;
    }
}

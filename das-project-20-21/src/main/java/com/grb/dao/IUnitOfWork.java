package com.grb.dao;

import com.grb.model.User;

public interface IUnitOfWork<T> {
    void registerNew(T entity, User user);
    void commit();
}

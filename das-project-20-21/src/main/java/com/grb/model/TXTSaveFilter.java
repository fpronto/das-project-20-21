package com.grb.model;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class TXTSaveFilter extends FileFilter {
    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return false;
        }

        String s = f.getName().toLowerCase();

        return s.endsWith(".txt");
    }

    @Override
    public String getDescription() {
        return "*.txt,*.TXT";
    }
}

package com.grb.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OriginalFile {

    public void addDocToDB(String documentPath, DataModel dm){
        String documentName = documentPath.substring(documentPath.lastIndexOf(File.separator)+1);
        documentName = documentName.substring(0, documentName.indexOf("."));
        Document document = new Document();
        document.setUser(dm.getUser());
        document.setName(documentName);
        document.setDirectory(documentPath);
        if(dm.getReferenceFile() != null) {
            document.setReferenceDirectory(dm.getReferenceFile().getFilePath());
        }
        dm.getUnitOfWork().registerNew(document, dm.getUser());
        dm.getUser().getDocument().add(document);
        var docs = dm.getDocumentsListModel();
        for (int i = 0; i< docs.getSize(); i++){
            if (docs.getElementAt(i).toString().equals(documentName)){
                return;
            }
        }
        dm.getDocumentsListModel().addElement(documentName);
    }

    public void createOriginalDoc(String text, String originalDocPath){
        try {
            FileWriter writer = new FileWriter(originalDocPath);
            writer.write(text);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prepareOriginalDoc(DataModel dm) {
        String fileName = dm.getFilename();
        Text text = dm.getText();
        String originalDocPath = System.getProperty("user.dir") + File.separator + fileName;
        originalDocPath = originalDocPath.substring(0, originalDocPath.lastIndexOf("."));
        originalDocPath = originalDocPath + ".txt";
        createOriginalDoc(text.getText(), originalDocPath);
        addDocToDB(originalDocPath, dm);
    }
}

package com.grb.model;

public enum SourceType {
    LIVRO,
    WEB_SITE,
    RELATÓRIO,
    JORNAL
}

package com.grb.model;

import com.grb.adapter.IFileFormatAdapter;
import com.grb.dao.UnitOfWork;
import com.grb.dao.UserRepository;

import javax.swing.*;
import java.util.ArrayList;

public class DataModel {
    UnitOfWork unitOfWork;
    ArrayList<Reference> referenceList;
    DefaultComboBoxModel documentsListModel;
    DefaultComboBoxModel userListModel;
    User user;          //used to save which user is controlling the system
    DefaultListModel model;
    int modifyIndex;
    IFileFormatAdapter referenceFile;
    String filename;
    Text text;
    boolean txt, html;
    String type;
    String extension;
    CommandManager cmd = new CommandManager();

    public DataModel() {
        unitOfWork = new UnitOfWork();
        referenceList = new ArrayList<>();
        documentsListModel = new DefaultComboBoxModel<>();
    }

    public UnitOfWork getUnitOfWork() {
        return unitOfWork;
    }

    public IFileFormatAdapter getReferenceFile() {
        return referenceFile;
    }

    public void setReferenceFile(IFileFormatAdapter referenceFile) {
        this.referenceFile = referenceFile;
    }

    public void redoAction (JTextArea text) {
        cmd.redo(text);
    }

    public void undoAction (JTextArea text) {
        cmd.undo(text);
    }

    public int existsUndo() {
        return cmd.existsUndo();
    }

    public int existsRedo() {
        return cmd.existsRedo();
    }

    public void addtoUndo(int c, String s) {
        CmText comm = new CmText(s,c);
        cmd.addToUndo(comm);
    }


    public void clearCmd() {
        cmd.clear();
    }
    public void setStyleInfo(String filename, String extension, Text text, String type) {
        this.filename = filename;
        this.extension = extension;
        this.text=text;
        this.type = type;
    }

    public String getExtension() {return extension;}

    public String getType() { return type;}

    public void setType(String type) {this.type = type;}

    public String getFilename() {return filename;}

    public void setFilename(String filename) {
        this.filename=filename;
    }

    public Text getText () {return text;}

    public void setText(Text text) {this.text = text;}

    public void setTxt(boolean b){
        txt = b;
    }

    public void setHtml(boolean b){
        html = b;
    }

    public boolean getTxt(){
        return txt;
    }

    public boolean getHtml(){
        return html;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getModifyIndex() {return modifyIndex;    }

    public void setModifyIndex(int modifyIndex) {
        this.modifyIndex = modifyIndex;
    }

    public void finishModify() {
        modifyIndex = -1;
    }

    public DefaultComboBoxModel getDocumentsListModel() {
        return documentsListModel;
    }

    public void setDocumentsListModel(DefaultComboBoxModel documentsListModel) {
        this.documentsListModel = documentsListModel;
    }

    public DefaultComboBoxModel getUserListModel() {
        return userListModel;
    }

    public void setUserListModel(DefaultComboBoxModel userListModel) {
        this.userListModel = userListModel;
    }

    public DefaultListModel getModel() {
        return model;
    }

    public void setModel(DefaultListModel model) {
        this.model = model;
    }

    public ArrayList<Reference> getReferenceList() {
        return referenceList;
    }

    public void setReferenceList(ArrayList<Reference> referenceList) {
        this.referenceList = referenceList;
    }



    public Reference getReference(int i){
        if(i>=0 && i < referenceList.size()){
            return referenceList.get(i);
        }
        return null;
    }
    public Reference getReference(String refCode){
        int index = referenceList.indexOf(refCode);
        for(Reference ref : referenceList) {
            if(ref.equals(new Reference.ReferenceBuilder().referenceCode(refCode).build())) {
                return ref;
            }
        }
        return new Reference.ReferenceBuilder().build();
    }
}

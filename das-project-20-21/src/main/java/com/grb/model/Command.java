package com.grb.model;

import javax.swing.*;

interface Command{
    public void redo(JTextArea textArea);
    public void undo(JTextArea textArea);
}


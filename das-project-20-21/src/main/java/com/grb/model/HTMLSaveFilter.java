package com.grb.model;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class HTMLSaveFilter extends FileFilter {
    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return false;
        }

        String s = f.getName().toLowerCase();

        return s.endsWith(".html");
    }

    @Override
    public String getDescription() {
        return "*.html,*.HTML";
    }
}

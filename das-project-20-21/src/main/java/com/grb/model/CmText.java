package com.grb.model;

import javax.swing.*;

public class CmText implements Command{
    String str;
    int position;

    public CmText(String str,int pos){
        this.str=str;
        this.position=pos;
    }
    @Override
    public void redo(JTextArea textArea){
        String text = textArea.getText();
        String aux = text.substring(0,position);
        aux += str;
        aux += text.substring(position,text.length());
        textArea.setText(aux);
    }
    @Override
    public void undo(JTextArea textArea) {
        String text = textArea.getText();
        String aux = text.substring(0,position);
        aux += text.substring(position+str.length(),text.length());
        textArea.setText(aux);
    }
}


package com.grb.model;

import com.grb.formater.Formater;
import com.grb.state.StateMachine;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class Text {
    StateMachine sm;
    JFrame app;
    String text;
    JList list;
    public String getText() {
        return text;
    }
    boolean bold, italic;

    public Text(String text, StateMachine sm, JFrame app) {
        this.sm = sm;
        this.text = text;
        this.app=app;
    }

    public void setList(JList l){
        list=l;
    }

    public void setBold(boolean l){
        bold=l;
    }

    public void setItalic(boolean l){
        italic=l;
    }

    public String getRef(String styleType, String formatType){
        int i=1;
        String returnText = text;
        List<Reference> listCods = new ArrayList();
        String regex = "\\\\cite\\{([A-Za-z0-9:\\- _]*)\\}";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(returnText);
        String str = "";
        while(m.find()) {
            str = m.group(0);
            String str2 = m.group(1);
            Reference ref = sm.getDm().getReference(str2);
            String replacingStr = "";
            Formater formater = new Formater
                    .FormaterBuilder()
                    .setReference(ref)
                    .setStyle(styleType)
                    .setExportFormat(formatType == "html")
                    .setBold(bold)
                    .setItalic(italic)
                    .build();
            if(!listCods.contains(ref)){
                listCods.add(ref);
                int index = listCods.indexOf(ref);
                replacingStr = formater.getCitation(index+1);
            }else {
                int index = listCods.indexOf(ref);
                replacingStr = formater.getCitation(index+1);
            }
            returnText=m.replaceFirst(replacingStr);
            m=p.matcher(returnText);
        }

        if(returnText.contains("\\insertRefList")) {
            String aux = "";
            String finalList = "";
            for(Reference ref : listCods) {
                Formater formater = new Formater
                        .FormaterBuilder()
                        .setReference(ref)
                        .setStyle(styleType)
                        .setExportFormat(formatType == "html")
                        .setBold(bold)
                        .setItalic(italic)
                        .build();
                int indexRef = listCods.indexOf(ref);
                finalList+=formater.formatRef(indexRef+1);
            }

            aux = "\\insertRefList";
            returnText=returnText.replace(aux,finalList);
        }
        return returnText;
    }
}

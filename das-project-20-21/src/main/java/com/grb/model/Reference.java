package com.grb.model;

import com.grb.adapter.RisFieldDefinitions;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class Reference {
    int index;
    String referenceCode;
    String authors;
    String articleName;
    SourceType sourceType;
    String source;
    String year;
    Map<String, List<String>> rawData = new HashMap<String, List<String>>();

    public Reference(ReferenceBuilder builder){
        this.index = builder.index;
        this.referenceCode = builder.referenceCode;
        this.authors = builder.authors;
        this.articleName = builder.articleName;
        this.sourceType = builder.sourceType;
        this.source = builder.source;
        this.year = builder.year;
    }

    public int getIndex() {
        return index;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public String getAuthors() {
        if(authors != null) {
            return authors;
        }
        return "";
    }

    public String getArticleName() {
        return articleName;
    }

    public String getSource() {
        if(source != null) {
            return source;
        }
        return "";
    }

    public String getYear() {
        if(year != null) {
            return year;
        }
        return "";
    }

    public SourceType getSourceType() {
        return sourceType;
    }


    @Override
    public String toString() {
        String finalString = "{" + referenceCode + "} - " + articleName;
        if(sourceType != null) {
            finalString += " " + sourceType + " ";
        }
        if(source != null) {
            finalString += " (" + source + ") ";
        }
        if(year != null) {
            finalString += " [" + year + "]";
        }


        return finalString;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Reference)) {
            return false;
        }
        Reference ref = (Reference) obj;

        return ref.getReferenceCode().equals(this.getReferenceCode());
    }

    @Override
    public int hashCode() {
        return this.getReferenceCode().hashCode();
    }

    public static class ReferenceBuilder {
        private int index;
        private String referenceCode;
        private String authors;
        private String articleName;
        private SourceType sourceType;
        private String source;
        private String year;

        public ReferenceBuilder(Reference reference){
            this.referenceCode = reference.getReferenceCode();
            this.authors = reference.getAuthors();
            this.articleName = reference.getArticleName();
            this.sourceType = reference.sourceType;
            this.source = reference.source;
            this.year = reference.year;
        }

        public ReferenceBuilder() {}

        public ReferenceBuilder index(int index){
            this.index = index;
            return this;
        }

        public ReferenceBuilder referenceCode(String referenceCode){
            this.referenceCode = referenceCode;
            return this;
        }

        public ReferenceBuilder authors(String authors){
            this.authors = authors;
            return this;
        }


        public ReferenceBuilder articleName(String articleName){
            this.articleName = articleName;
            return this;
        }

        public ReferenceBuilder sourceType(SourceType sourceType){
            this.sourceType = sourceType;
            return this;
        }

        public ReferenceBuilder source(String source){
            this.source = source;
            return this;
        }

        public ReferenceBuilder year(String year){
            this.year = year;
            return this;
        }

        public Reference build() {
            Reference ref =  new Reference(this);
            return ref;
        }
    }
}

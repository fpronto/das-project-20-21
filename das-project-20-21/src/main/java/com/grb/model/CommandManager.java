package com.grb.model;

import com.grb.ui.App;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class CommandManager {
    List<Command> undoList= new ArrayList<Command>();
    List<Command> redoList = new ArrayList<Command>();
    public CommandManager(){
    }
    public int redo(JTextArea textArea){
        if(redoList.isEmpty())
            return 0;
        Command last=redoList.remove(redoList.size()-1);
        last.redo(textArea);
        undoList.add(last);
        if(redoList.size()>0)
            return 1;
        else
            return 0;
    }
    public int undo(JTextArea textArea){
        if(undoList.isEmpty())
            return 0;
        Command last=undoList.remove(undoList.size()-1);
        last.undo(textArea);
        redoList.add(last);
        if(undoList.size()>0) {
            return 1;
        }else {
            return 0;
        }
    }

    public int existsUndo() {
        if(undoList.size()>0) {
            return 1;
        } else {
            return 0;
        }
    }

    public int existsRedo() {
        if(redoList.size()>0) {
            return 1;
        } else {
            return 0;
        }
    }


    public void addToUndo(Command c){
        undoList.add(c);
    }

    public void clear(){ redoList = new ArrayList<Command>();}
}

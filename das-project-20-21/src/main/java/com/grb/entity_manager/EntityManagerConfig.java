package com.grb.entity_manager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerConfig {
    private static EntityManagerFactory emf;

    public static EntityManager getEntityManager(){
        if(emf == null){
            emf = Persistence.createEntityManagerFactory("Default");
        }
        return emf.createEntityManager();
    }

    public void close(){
        emf.close();
    }
}

package com.grb.formater;

public class ItalicHtml extends HtmlFormater{
    @Override
    public String formatRef(int i, String formatedInfo) {
        return "<i>"+formatedInfo+"</i>";
    }
}

package com.grb.formater;

import com.grb.model.Reference;

public class APA extends Styler{
    @Override
    public String formatRef(Reference ref, int i) {
        String str = "";
        if(!ref.getAuthors().equals("")){
            str+= ref.getAuthors() + " ";
        }
        if(!ref.getYear().equals("")){
            str+= "(" + ref.getYear()+") ";
        }
        if(!ref.getArticleName().equals("")){
            str+= ref.getArticleName() + " ";
        }
        if(!ref.getSource().equals("")){
            str+= ref.getSource();
        }

        return  str + "\n";
    }
    @Override
    public String formatCitation(Reference ref, int i) {
        return "(" + ref.getAuthors() + "," + ref.getYear() + ")";
    }
}

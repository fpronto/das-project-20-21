package com.grb.formater;

import com.grb.model.Reference;

public class BaseHtml extends HtmlFormater{
    @Override
    public String formatRef(int i, String formatedInfo) {
        return "<p id=[\""+i+"\"]>"+formatedInfo+"</p>";
    }
    @Override
    public String formatCitation(int i, String formatedInfo) {
        return "<a href=\"#["+i+"]\">"+formatedInfo+"</a>";
    }
}

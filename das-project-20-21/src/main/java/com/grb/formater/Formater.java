package com.grb.formater;

import com.grb.model.Reference;
import com.grb.model.SourceType;

public class Formater {
    private Styler style;
    private boolean html;
    private boolean italic;
    private boolean bold;
    private Reference ref;

    public Formater(Formater.FormaterBuilder builder){
        this.style = builder.style;
        this.html= builder.html;
        this.italic = builder.italic;
        this.bold = builder.bold;
        this.ref= builder.ref;
    }

    public String formatRef(int i) {
        String returnValue = "";
        returnValue = style.formatRef(this.ref, i);
        if(html) {
            if(bold) {
                returnValue = new BoldHtml().formatRef(i, returnValue);
            }
            if(italic) {
                returnValue = new ItalicHtml().formatRef(i, returnValue);
            }
            returnValue = new BaseHtml().formatRef(i, returnValue);
        }
        return returnValue;
    }
    public String getCitation(int i) {
        String returnValue = "";
        returnValue = style.formatCitation(this.ref, i);
        if(html) {
            returnValue = new BaseHtml().formatCitation(i, returnValue);
        }
        return returnValue;
    }

    public static class FormaterBuilder{
        private Styler style;
        private boolean html = false;
        private boolean italic = false;
        private boolean bold = false;
        private Reference ref;

        public FormaterBuilder() {}

        public FormaterBuilder setStyle(String style) {
            switch (style) {
                case "ACM":
                    this.style = new ACM();
                    break;
                case "APA":
                    this.style = new APA();
                    break;
                case "IEEE":
                    this.style = new IEEE();
                    break;
            }
            return this;
        }

        public FormaterBuilder setItalic(boolean italic) {
            this.italic = italic;
            return this;
        }

        public FormaterBuilder setBold(boolean bold) {
            this.bold = bold;
            return this;
        }
        public FormaterBuilder setReference(Reference ref) {
            this.ref = ref;
            return this;
        }

        public FormaterBuilder setExportFormat(boolean html) {
            this.html = html;
            return this;
        }
        public Formater build() {
            Formater formater =  new Formater(this);
            return formater;
        }
    }
}

package com.grb.formater;

import com.grb.model.Reference;

public class IEEE extends Styler{
    @Override
    public String formatRef(Reference ref, int i) {
        String str = "[" + i + "]";
        if(!ref.getAuthors().equals("")){
            str+= " " + ref.getAuthors();
        }
        if(!ref.getArticleName().equals("")){
            str+= ", \"" + ref.getArticleName() + "\"";
        }
        if(!ref.getSource().equals("")){
            str+= ", " + ref.getSource();
        }
        if(!ref.getYear().equals("")){
            str+= ", " + ref.getYear();
        }
        return  str + "\n";
    }

    @Override
    public String formatCitation(Reference ref, int i) {
        return "[" + i + "]";
    }
}

package com.grb.state;

import com.grb.model.DataModel;

public class InsertReferenceState extends AbstractState{

    InsertReferenceState(DataModel dm) {
        super(dm);
    }

    @Override
    public IState ok() {
        return new ManageReferencesState(dm);
    }

    @Override
    public IState cancel() {
        return new ManageReferencesState(dm);
    }

    @Override
    public String getState() {
        return "Insert Reference";
    }
}

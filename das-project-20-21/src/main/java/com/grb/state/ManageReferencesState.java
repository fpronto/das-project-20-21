package com.grb.state;

import com.grb.model.DataModel;

public class ManageReferencesState extends AbstractState{

    ManageReferencesState(DataModel dm) {
        super(dm);
    }

    @Override
    public IState openAddReference() {
        return new AddReferenceState(dm);
    }

    @Override
    public IState openModifyReference(int referenceIndex) {
        dm.setModifyIndex(referenceIndex);
        return new ModifyReferenceState(dm);
    }

    @Override
    public IState deleteReference(int referenceIndex) {
        dm.getReferenceList().remove(referenceIndex);
        dm.getModel().remove(referenceIndex);
        dm.getReferenceFile().updateReferenceFile(dm.getReferenceList());
        return this;
    }

    @Override
    public IState close() {
        return new InitialState(dm);
    }

    @Override
    public String getState() {
        return "Manage References";
    }
}

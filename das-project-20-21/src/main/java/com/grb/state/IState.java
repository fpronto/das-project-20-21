package com.grb.state;


import com.grb.model.Reference;
import com.grb.model.Text;
import com.grb.model.User;

import javax.swing.*;

public interface IState{
    IState loadReferences(JFrame parent) throws Exception;
    IState loadReferences(String filePath) throws Exception;
    IState openManageReferences();
    IState openStyle();
    IState openAddReference();
    IState openModifyReference(int referenceIndex);
    IState openAddUser();
    IState ok();
    IState ok(Reference ref);
    IState ok(User user);
    IState ok(boolean bold, boolean italic);
    IState cancel();
    IState deleteReference(int referenceIndex);
    IState openInsertReference();
    IState redoAction(JTextArea text);
    IState undoAction(JTextArea text);
    IState exportAction(Text text, String type, String fileName, String ext);
    IState close();
    String getState();
}
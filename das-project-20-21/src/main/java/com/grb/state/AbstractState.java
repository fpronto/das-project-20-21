package com.grb.state;

import com.grb.model.DataModel;
import com.grb.model.Reference;
import com.grb.model.Text;
import com.grb.model.User;

import javax.swing.*;

public class AbstractState implements IState {
    DataModel dm;

    AbstractState(DataModel dm) {
        this.dm = dm;
    }

    @Override
    public IState loadReferences(JFrame parent) throws Exception {
        return null;
    }

    @Override
    public IState loadReferences(String filePath) throws Exception {
        return null;
    }

    @Override
    public IState openManageReferences() {
        return this;
    }

    @Override
    public IState openStyle() {
        return this;
    }

    @Override
    public IState openAddReference() {
        return this;
    }

    @Override
    public IState openModifyReference(int referenceIndex) {
        return this;
    }

    @Override
    public IState openAddUser() {
        return this;
    }

    @Override
    public IState ok() {
        return this;
    }

    @Override
    public IState ok(Reference ref) {
        return this;
    }

    @Override
    public IState ok(User user) {
        return this;
    }

    @Override
    public IState ok(boolean bold, boolean italic) {
        return this;
    }


    @Override
    public IState cancel() {
        return this;
    }

    @Override
    public IState deleteReference(int referenceIndex) {
        return this;
    }

    @Override
    public IState openInsertReference() {
        return this;
    }

    @Override
    public IState redoAction(JTextArea text) {
        return this;
    }

    @Override
    public IState undoAction(JTextArea text) {
        return this;
    }

    @Override
    public IState exportAction(Text text, String type, String fileName, String ext) {
        return this;
    }

    @Override
    public IState close() {
        return this;
    }

    @Override
    public String getState() {
        return "This";
    }
}

package com.grb.state;
import com.grb.dao.UserRepository;
import com.grb.model.DataModel;
import com.grb.model.User;

public class AddUserState extends AbstractState{

    AddUserState(DataModel dm) {
        super(dm);
    }

    @Override
    public IState ok(User user) {
        dm.getUserListModel().addElement(user.getName());
        UserRepository userRepository = new UserRepository();
        userRepository.addUser(user);
        return new InitialState(dm);
    }

    @Override
    public IState close() {
        return new InitialState(dm);
    }
}

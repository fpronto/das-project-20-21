package com.grb.state;

import com.grb.adapter.FactoryReferenceFile;
import com.grb.adapter.IFileFormatAdapter;
import com.grb.dao.DocumentRepository;
import com.grb.model.*;

import javax.swing.*;
import java.io.*;


public class InitialState extends AbstractState{
    InitialState(DataModel dm) {
        super(dm);
    }

    @Override
    public IState loadReferences(JFrame parent) throws Exception {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        int result = fileChooser.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            String filePath = selectedFile.getAbsolutePath();
            loadReferences(filePath);
            /*IFileFormatAdapter fileFormat = new FactoryReferenceFile().getFile(filePath);
            dm.setReferenceFile(fileFormat);
            dm.setReferenceList(fileFormat.parseFile());
            if (dm.getReferenceList().isEmpty())
                throw new Exception();*/
        }
        else if (result == JFileChooser.CANCEL_OPTION){
            throw new Exception();
        }
        return this;
    }

    public IState loadReferences(String filePath) throws Exception {
        IFileFormatAdapter fileFormat = new FactoryReferenceFile().getFile(filePath);
        dm.setReferenceFile(fileFormat);
        dm.setReferenceList(fileFormat.parseFile());
        if (dm.getReferenceList().isEmpty())
            throw new Exception();
        return this;
    }

    @Override
    public IState openAddUser() {
        return new AddUserState(dm);
    }

    @Override
    public IState openManageReferences() {
        return new ManageReferencesState(dm);
    }

    @Override
    public IState redoAction(JTextArea text) {
        dm.redoAction(text);
        return this;
    }

    @Override
    public IState undoAction(JTextArea text) {
        dm.undoAction(text);
        return this;
    }

    @Override
    public IState exportAction(Text text, String type, String fileName, String ext) {

        if (ext == ".html") {
            return new StyleState(dm);
        }

        if (fileName != null) {
            try {
                dm.setFilename(fileName.substring(fileName.lastIndexOf(File.separator)+1));
                OriginalFile createOriginalFile = new OriginalFile();
                createOriginalFile.prepareOriginalDoc(dm);

                String str = text.getRef(type, ext);
                String finalText = str;
                FileWriter myWriter = new FileWriter(fileName);
                myWriter.write(finalText);
                myWriter.close();


                /*String originalDocPath = System.getProperty("user.dir") + File.separator + fileName.substring(fileName.lastIndexOf(File.separator)+1);
                originalDocPath = originalDocPath.substring(0, originalDocPath.lastIndexOf("."));
                originalDocPath = originalDocPath + ".txt";
                createOriginalDocument(text.getText(), originalDocPath);
                addDocumentToDB(originalDocPath);*/

            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        return this;
    }

    @Override
    public String getState() {
        return "Initial State";
    }
}

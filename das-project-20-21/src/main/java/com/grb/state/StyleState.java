package com.grb.state;

import com.grb.dao.DocumentRepository;
import com.grb.model.DataModel;
import com.grb.model.Document;
import com.grb.model.OriginalFile;
import com.grb.model.Text;
import com.grb.ui.Style;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class StyleState extends AbstractState{
    Style style;
    StyleState(DataModel dm) {
        super(dm);
    }

    @Override
    public IState ok(boolean bold, boolean italic) {
        String fileName = dm.getFilename();
        Text text = dm.getText();
        String type = dm.getType();

        if (fileName != null) {
            try {
                dm.setFilename(fileName.substring(fileName.lastIndexOf(File.separator)+1));
                OriginalFile createOriginalFile = new OriginalFile();
                createOriginalFile.prepareOriginalDoc(dm);

                String ext = "html";
                text.setBold(bold);
                text.setItalic(italic);

                String str = text.getRef(type, ext);
                String finalText = str;
                FileWriter myWriter = new FileWriter(fileName);
                finalText = "<html><body><p>";
                str=str.replace("\n","</p><p>");
                finalText += str +"</body></html>";
                finalText = finalText.replace("<p></body>", "</body>");
                myWriter.write(finalText);
                myWriter.close();



                /*String originalDocPath = System.getProperty("user.dir") + File.separator + fileName.substring(fileName.lastIndexOf(File.separator)+1);
                originalDocPath = originalDocPath.substring(0, originalDocPath.lastIndexOf("."));
                originalDocPath = originalDocPath + ".txt";
                createOriginalDocument(text.getText(), originalDocPath);
                addDocumentToDB(originalDocPath);*/
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        return new InitialState(dm);
    }

    @Override
    public IState cancel() {
        return new InitialState(dm);
    }

    @Override
    public String getState() {
        return "Style";
    }
}
package com.grb.state;

import com.grb.adapter.IFileFormatAdapter;
import com.grb.model.DataModel;
import com.grb.model.Reference;

import java.util.ArrayList;

public class ModifyReferenceState extends AbstractState{

    ModifyReferenceState(DataModel dm) {
        super(dm);
    }

    @Override
    public IState ok(Reference ref) {
        dm.getReferenceList().set(dm.getModifyIndex(), ref);
        ArrayList<Reference> ax = dm.getReferenceList();
        dm.getModel().set(ref.getIndex(), ref.toString());
        dm.getReferenceFile().updateReferenceFile(dm.getReferenceList());
        dm.finishModify();
        return new ManageReferencesState(dm);
    }

    @Override
    public IState close() {
        dm.finishModify();
        return new ManageReferencesState(dm);
    }

    @Override
    public IState cancel() {
        return new ManageReferencesState(dm);
    }

    @Override
    public String getState() {
        return "Modify Reference";
    }
}

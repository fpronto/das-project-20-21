package com.grb.state;

import com.grb.model.DataModel;
import com.grb.model.Reference;

public class AddReferenceState extends AbstractState {
    AddReferenceState(DataModel dm) {
        super(dm);
    }

    @Override
    public IState ok(Reference ref) {
        dm.getReferenceList().add(ref);
        dm.getModel().addElement(ref.toString());
        dm.getReferenceFile().updateReferenceFile(dm.getReferenceList());
        return new ManageReferencesState(dm);
    }

    @Override
    public IState close() {
        dm.finishModify();
        return new ManageReferencesState(dm);
    }

    @Override
    public IState cancel() {
        return new ManageReferencesState(dm);
    }

    @Override
    public String getState() {
        return "Add Reference";
    }
}

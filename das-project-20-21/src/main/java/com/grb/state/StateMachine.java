package com.grb.state;

import com.grb.model.*;
import com.grb.ui.*;

import javax.swing.*;
import java.io.File;

public class StateMachine {
    private IState currentState;
    private App application;
    private JFrame manageReferenceDialog;
    private JFrame InsertReferecesDialog;
    private InsertReferences insert;
    private JFrame addUserDialog;
    private DataModel dm;
    private JFrame style;

    public DataModel getDm() {
        return dm;
    }

    public int existsUndo() {
        return dm.existsUndo();
    }
    public int existsRedo() {
        return dm.existsRedo();
    }

    public void addToUndo(int c, String s) {
        dm.addtoUndo(c, s);
    }

    public void clearCmd() {
        dm.clearCmd();
    }
    public void setDm(DataModel dm) {
        this.dm = dm;
    }

    public void setCurrentState(IState currentState) {
        this.currentState = currentState;
    }

    public StateMachine() {
        dm = new DataModel();
        setCurrentState(new InitialState(dm));
        application = new App(this);
    }

    IState getCurrentState() {
        return currentState;
    }

    public void loadReferences(JFrame parent) throws Exception {
        try {
            setCurrentState(currentState.loadReferences(parent));
        } catch (Exception e) {
            if (dm.getReferenceList().isEmpty())
                throw e;
        }
    };

    public void loadReferences(String filePath) throws Exception {
        currentState.loadReferences(filePath);
    }

    public void openManageReferences() {
        if(getCurrentState() instanceof InitialState) {
            manageReferenceDialog = new ManageReferences(application, this);
        }
        setCurrentState(currentState.openManageReferences());
    };

    public void openStyle() {

        setCurrentState(currentState.openStyle());
    };

    public void openInsertReferences() {
        if(getCurrentState() instanceof InitialState) {
            InsertReferecesDialog = new InsertReferences(application, this);
        }
        setCurrentState(currentState.openInsertReference());
    };


    public void openAddUser(){
        if(getCurrentState() instanceof InitialState) {
            addUserDialog = new addUser(application, this);
        }
        setCurrentState(currentState.openAddUser());
    }

    public void openAddReference() {
        if(getCurrentState() instanceof ManageReferencesState ) {
            setCurrentState(currentState.openAddReference());
            new AddReference(manageReferenceDialog, this);
        } else {
            setCurrentState(currentState.openAddReference());
        }
    };

    public void insertReference(int index, int cursor) {
        String finalText = "\\insertRefList";
        Reference ref = dm.getReference(index);
        if(ref != null) {
            finalText = "\\cite{"+ref.getReferenceCode()+"}";
        }

        application.insertReference(application.getCurrentPosition(),finalText);
    };

    public void setInsert(InsertReferences i){
        this.insert = i;
    }

    public JList getList(){
        return this.insert.getList();
    }

    public int getCurrentPosition(){
        return application.getCurrentPosition();
    }

    public void openModifyReference(int referenceIndex) {
        if(getCurrentState() instanceof ManageReferencesState ) {
            setCurrentState(currentState.openModifyReference(referenceIndex));
            new ModifyReference(manageReferenceDialog, this);
        } else {
            setCurrentState(currentState.openModifyReference(referenceIndex));
        }
    };

    public void ok(Reference ref) {
        setCurrentState(currentState.ok(ref));
    };

    public void ok(User user){
        setCurrentState(currentState.ok(user));
    }

    public void ok(boolean bold, boolean italic){
        setCurrentState(currentState.ok(bold, italic));
    }

    public void ok() {
        setCurrentState(currentState.ok());
    };

    public void cancel() {
        setCurrentState(currentState.cancel());
    };

    public void deleteReference(int referenceIndex) {
        setCurrentState(currentState.deleteReference(referenceIndex));
    };

    public void openInsertReference() {
        setCurrentState(currentState.openInsertReference());
    };

    public void redoAction(JTextArea text) {
        setCurrentState(currentState.redoAction(text));
    };

    public void undoAction(JTextArea text) {
        setCurrentState(currentState.undoAction(text));
    };

    public void close() {
        setCurrentState(currentState.close());
    }

    public void exportAction(Text text, String type ) {
        String ext = "";
        String fileName = null;

        if(getCurrentState() instanceof InitialState) {
            JFileChooser fc = new JFileChooser();
            fc.addChoosableFileFilter(new TXTSaveFilter());
            fc.addChoosableFileFilter((new HTMLSaveFilter()));
            fc.setAcceptAllFileFilterUsed(false);
            int retrieval = fc.showSaveDialog(null);


            if (retrieval == JFileChooser.APPROVE_OPTION) {
                String extension = fc.getFileFilter().getDescription();

                if (extension.equals("*.txt,*.TXT")) {
                    ext = ".txt";
                } else if (extension.equals("*.html,*.HTML")) {
                    ext = ".html";
                }

                fileName = fc.getCurrentDirectory().toString();
                fileName = fileName.concat(File.separator + fc.getSelectedFile().getName());

                if (ext == ".txt" && !fileName.endsWith(".txt"))
                    fileName = fileName.concat(ext);
                if (ext == ".html") {
                    if(!fileName.endsWith(".html")) {
                        fileName = fileName.concat(ext);
                    }
                    dm.setStyleInfo(fileName, ext, text, type);
                    style = new Style(application, this);
                }
                dm.setFilename(fileName);
                dm.setText(text);
                setCurrentState(currentState.exportAction(text, type, fileName, ext));
            }
        }
}

    public String getCurentState() {return currentState.toString();}

    public void setModel(DefaultListModel<String> model) {
        getDm().setModel(model);
    }

    public void setUsersListModel(DefaultComboBoxModel<String> model){
        getDm().setUserListModel(model);
    }

    public void setDocumentsListModel(DefaultComboBoxModel<String> model){
        getDm().setDocumentsListModel(model);
    }

    public Reference getReferenceToModify() {
        int referenceIndex = dm.getModifyIndex();
        Reference reference = dm.getReferenceList().get(referenceIndex);
        reference = new Reference.ReferenceBuilder(reference).index(referenceIndex).build();
        dm.getReferenceList().set(referenceIndex, reference);
        return reference;
    }
}
